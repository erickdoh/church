<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        DB::table("members")->insert([
            'fname'=>'admin',
            'lname'=>'admin',
            'phone'=>'0797909406',
            'email'=>'admin@gmail.com',
            'group'=>'admin',
            'password'=>bcrypt('password'),
            'image'=>'Null',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}

