<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Couple extends Model
{
    use HasFactory;

    protected $fillable=[
        'hfname',
        'hlname',
        'hemail',
        'himage',
        'wfname',
        'wlname',
        'wemail',
        'wimage',
    ];
}