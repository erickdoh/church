<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Members;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;

class AuthController extends Controller
{
     //login
     public function login(Request $request){

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message'=>'Invalid email or password'];
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return
        [ 'message'=>'Successful login', 'status'=>true,'user'=>$user, 'access_token' => $tokenResult->accessToken,  'token_type' => 'Bearer',  'expires_at' => Carbon::parse(  $tokenResult->token->expires_at )->toDateTimeString()
        ];
    }
}
