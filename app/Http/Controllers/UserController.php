<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Group;
use App\Models\Couple;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;



class UserController extends Controller
{

    public function home(){
        return view('layouts.vue.blade');
    }
    public function register(Request $request){

        $data = $request->validate([
            'image'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
            'fname'     => ['required', 'string'],
            'lname'    => ['required', 'string'],
            'gender'     => ['required', 'string'],
            'dateOfBirth'    => ['required'],
            'district'    => ['required', 'string'],
            'phone' => ['required',],
            'email'=> ['required', 'email'],
            'group'=> ['required', 'string'],
            'testimony'=> ['required', 'string'],
        ]);

        $file = $request->file('image');
        $name = '/image/' . uniqid() . '.' . $file->extension();
        $file->storePubliclyAs('public', $name);
        $data['image'] = $name;

        $user = User::create($data);
        return ['status' => true, 'message' => 'Submitted Successfully'];
    }

     //login
     public function login(Request $request){

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message'=>'Invalid email or password'];
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return
        [ 'message'=>'Successful login', 'status'=>true,'user'=>$user, 'access_token' => $tokenResult->accessToken,  'token_type' => 'Bearer',  'expires_at' => Carbon::parse(  $tokenResult->token->expires_at )->toDateTimeString()
        ];
    }

    public function allusers()
    {
        $users = User::all()->where('group','!=', 'admin');
        return ['status' => true, 'list' => $users];
    }

    public function deluser($id)
    {
        $user = User::find($id);
    //    $filename = User::where('id', $id)->pluck('image')->all();
       unlink(storage_path('app/public/'.$user->image));
        $user->delete();
       
        return ['status' => true, 'message' => 'Successfully deleted'];
    }

    public function viewuser($id)
    {
        $user = User::find($id);
        return ['status' => true, 'user' => $user];
    }

    //logout
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return ['status' => true, 'message' => 'Logged out'];
    }

   public function addgroup(Request $request){
      $group=Group::create($request->all());
      return ['status' => true, 'message' => 'New group added successfully'];
   }

   public function allgroups(){
       $group=Group::all();
       return ['status' => true, 'list' => $group];
   }

   public function editgroup($id){
     $group = Group::find($id);
     return ['status' => true, 'group' => $group];
   }

   public function updateGroup(Request $request, $id){
    $group = Group::find($id);
    $group->update($request->all());
   return['status'=>true,'message'=>'Successfully updated'];
  }

  public function delgroup($id){
    $group = Group::find($id);
    $group->delete();
    return['status'=>true, 'message'=>'Group deleted'];      
  }

  public function regcouple(Request $request){

    $data = $request->validate([    
        
        'hfname'     => ['required', 'string'],
        'hlname'    => ['required', 'string'],
        'hemail'=> ['required', 'email'],
        'himage'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
        'wfname'     => ['required', 'string'],
        'wlname'    => ['required', 'string'],
        'wemail'=> ['required', 'email'],
        'wimage'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
    ]);

    $file = $request->file('himage');
    $hname = '/image/' . uniqid() . '.' . $file->extension();
    $file->storePubliclyAs('public', $hname);
    $data['himage'] = $hname;

    $file = $request->file('wimage');
    $wname = '/image/' . uniqid() . '.' . $file->extension();
    $file->storePubliclyAs('public', $wname);
    $data['wimage'] = $wname;

    $user = Couple::create($data);
    return ['status' => true, 'message' => 'Submitted Successfully'];
  }

  public function allcouples(){
    $couples = Couple::all();
    return ['status' => true, 'list' => $couples];
  }
}
